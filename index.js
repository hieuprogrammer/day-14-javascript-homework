/* BÀI TẬP 01 */
document.getElementById(`btnInKetQua1`).addEventListener(`click`, function() {
    var soThuNhat = document.getElementById(`soThuNhat_b1`).value;
    var soThuHai = document.getElementById(`soThuHai_b1`).value;
    var soThuBa = document.getElementById(`soThuBa_b1`).value;

    var soDaNhap = [soThuNhat, soThuHai, soThuBa];

    // soDaNhap.sort(); // Using JavaScript's built-in sort() method is too easy to be practicing.
    for (var i = 0; i < soDaNhap.length; i++) {
        for (var j = i + 1; j < soDaNhap.length; j++) {
            var temp = 0;
            if (soDaNhap[i] > soDaNhap[j]) {
                temp = soDaNhap[i];
                soDaNhap[i] = soDaNhap[j];
                soDaNhap[j] = temp;    
            }
        }
    }

    document.getElementById(`ketQuaBai1`).innerHTML = soDaNhap;
});

/* BÀI TẬP 02 */
document.getElementById(`btnInKetQua2`).addEventListener(`click`, function() {
    var soThuNhat = document.getElementById(`soThuNhat_b2`).value;
    var soThuHai = document.getElementById(`soThuHai_b2`).value;
    var soThuBa = document.getElementById(`soThuBa_b2`).value;

    var soDaNhap = [soThuNhat, soThuHai, soThuBa];

    var max = soDaNhap[0];

    for (var index = 0; index < soDaNhap.length; index++) {
        if (soDaNhap[index] > max) {
            max = soDaNhap[index];
        }
    }

    document.getElementById(`ketQuaBai2`).innerHTML = max;
});

/* BÀI TẬP 03 */
document.getElementById(`btnInKetQua3`).addEventListener(`click`, function() {
    var soThuNhat = document.getElementById(`soThuNhat_b3`).value;
    var soThuHai = document.getElementById(`soThuHai_b3`).value;
    var soThuBa = document.getElementById(`soThuBa_b3`).value;
    var count = 0;

    var soDaNhap = [soThuNhat, soThuHai, soThuBa];
    var soChanDaNhap = [];

    for (var index = 0; index < soDaNhap.length; index++) {
        if (soDaNhap[index] % 2 == 0) {
            count++;
            soChanDaNhap.push(soDaNhap[index]);
        }
    }

    document.getElementById(`ketQuaBai3`).innerHTML = `Bạn đã nhập ${count} số chẵn là các số: ${soChanDaNhap}.`;
});